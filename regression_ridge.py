
import numpy as np
import math
import matplotlib.pyplot as plt

# X = np.random.rand(100)
# epsilon = np.random.normal(0, math.sqrt(2), 100)
# T = X ** 3 - 3 * X ** 2 - X + 3 + epsilon
# np.save('data', [X, epsilon, T])


from sklearn.linear_model import Ridge
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error

X, epsilon, T = np.load('data.npy')
X = np.array([X**i for i in range(0, 11)]).transpose()

alphas = np.logspace(-8, 4, 13)

rmse_train_total = []
rmse_test_total = []

for _ in range(0, 100):
	X_train, X_test, T_train, T_test = train_test_split(X, T, test_size=0.7)

	rmse_train = []
	rmse_test = []

	for alpha in alphas:
		ridge = Ridge(alpha=alpha)
		ridge.fit(X_train, T_train)

		pred_train = ridge.predict(X_train)
		rmse_train.append(math.sqrt(mean_squared_error(T_train, pred_train)))

		pred_test = ridge.predict(X_test)
		rmse_test.append(math.sqrt(mean_squared_error(T_test, pred_test)))

	rmse_train_total.append(rmse_train)
	rmse_test_total.append(rmse_test)

ax = plt.gca()
ax.plot(alphas, np.mean(rmse_train_total, axis=0), 'b')
ax.plot(alphas, np.mean(rmse_test_total, axis=0), 'r')
plt.xscale('log')
plt.xlabel('Lnλ')
plt.ylabel('RMSE')
plt.legend(['Train', 'Test'])
plt.title('Ridge error as a function of the regularization')
plt.axis('tight')
plt.show()
	

