
import numpy as np
import math
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import Ridge

L_SIZE = 50
X_SIZE = 200

DX = []
DT = []

for i in range(0, L_SIZE):
	X = np.random.uniform(-3, 3, X_SIZE)
	epsilon = np.random.normal(0, math.sqrt(2), X_SIZE)
	T = X ** 3 - 3 * X ** 2 - X + 3 + epsilon

	DX.append(X)
	DT.append(T)


np.save('data2', [DX, DT])

DX, DT = np.load('data2.npy')

alphas = np.logspace(-8, 4, 13)

rmse = []
bias = []
variance = []

hx = np.mean(DT, axis=0)

for alpha in alphas:

	pred_array = []
	rmse_array = []

	for X, T in zip(DX, DT):

		phi_X = np.array([X**i for i in range(0, 11)]).transpose()

		ridge = Ridge(alpha=alpha)
		ridge.fit(phi_X, T)

		pred = ridge.predict(phi_X)
		rmse_array.append(math.sqrt(mean_squared_error(T, pred)))

		pred_array.append(pred)

	yhat = np.mean(pred_array, axis=0)
	bias.append(mean_squared_error(yhat, hx))

	rmse.append(np.mean(rmse_array, axis=0))
	
	var = []
	for pred in pred_array:
		var.append(mean_squared_error(pred, yhat))

	variance.append(np.mean(var) / X_SIZE)


ax = plt.gca()
ax.plot(alphas, bias, 'r')
ax.plot(alphas, variance, 'g')
ax.plot(alphas, rmse, 'b')
plt.xscale('log')
plt.xlabel('Lnλ')
plt.legend(['Bias^2', 'Variance', 'RMSE'])
plt.title('Ridge error, bias and variance as a function of the regularization')
plt.axis('tight')
plt.show()
	
