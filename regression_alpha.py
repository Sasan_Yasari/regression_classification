from sklearn.model_selection import KFold
import numpy as np
import math
import matplotlib.pyplot as plt
from sklearn.linear_model import Ridge
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split


X, epsilon, T = np.load('data.npy')
X = np.array([X**i for i in range(0, 11)]).transpose()

kf = KFold(n_splits=10)

ax = plt.gca()

alphas = np.logspace(-8, 4, 13)

rmse_train_total = []
rmse_validation_total = []
rmse_test_total = []

for _ in range(0, 100):

	X_train, X_test, T_train, T_test = train_test_split(X, T, test_size=0.7)

	# print(_)

	fold_rmse_train = []
	fold_rmse_validation = []
	fold_rmse_test = []	

	for train_index, validation_index in kf.split(X_train):
		X_train, X_validation = X[train_index], X[validation_index] 
		T_train, T_validation = T[train_index], T[validation_index]

		rmse_train = []
		rmse_validation = []
		rmse_test = []

		for alpha in alphas:
			ridge = Ridge(alpha=alpha)
			ridge.fit(X_train, T_train)
			
			pred_train = ridge.predict(X_train)
			rmse_train.append(math.sqrt(mean_squared_error(T_train, pred_train)))

			pred_validation = ridge.predict(X_validation)
			rmse_validation.append(math.sqrt(mean_squared_error(T_validation, pred_validation)))

			pred_test = ridge.predict(X_test)
			rmse_test.append(math.sqrt(mean_squared_error(T_test, pred_test)))

		fold_rmse_train.append(rmse_train)
		fold_rmse_validation.append(rmse_validation)
		fold_rmse_test.append(rmse_test)

	rmse_train_total.append(fold_rmse_train)
	rmse_validation_total.append(fold_rmse_validation)
	rmse_test_total.append(fold_rmse_test)

rmse_train_total = np.array(rmse_train_total)
rmse_validation_total = np.array(rmse_validation_total)
rmse_test_total = np.array(rmse_test_total)

train_mean = np.mean(rmse_train_total)
validation_mean = np.mean(rmse_validation_total)
test_mean = np.mean(rmse_test_total)

ax.plot(alphas, train_mean, 'b')
ax.plot(alphas, validation_mean, 'g')
ax.plot(alphas, test_mean, 'r')

print('BEST λ : ' + str(alphas[np.argmin(test_mean_final)]))
print('VALIDATION AMOUNT FOR BEST λ : ' + str(validation_mean_final[np.argmin(test_mean_final)]))

plt.xscale('log')
plt.xlabel('Lnλ')
plt.ylabel('RMSE')
plt.legend(['Train', 'Validation', 'Test'])
plt.title('Ridge error as a function of the regularization')
plt.axis('tight')
plt.show()

