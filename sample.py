import numpy as np
import matplotlib.pyplot as plt

x = np.arange(-3, 2)

plt.xlabel('x')
plt.ylabel('y')

plt.plot(x, 2*x-1)
plt.plot([-2, -1, 0, 1], [-1, 1, 8, 5], 'ro')
plt.plot(x, 2.5*x+4.5, 'g-')

plt.legend(['y = 2x-1', 'Datas', 'Desired Model'])
plt.savefig('1')
plt.show()